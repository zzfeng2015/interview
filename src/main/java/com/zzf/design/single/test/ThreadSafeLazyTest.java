package com.zzf.design.single.test;

import com.zzf.design.single.lazy.LazyOne;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * 模拟并发, 懒汉式不安全
 * @author zzf
 * @date 2018/8/24 23:49.
 */
public class ThreadSafeLazyTest {

    public static void main(String[] args) {
        int count = 100;
        CountDownLatch latch = new CountDownLatch(count);

        final Set<LazyOne> synSet = Collections.synchronizedSet(new HashSet<>());

        long start = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
//            new Thread(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        latch.await();//countdown = 0 ，释放
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }

                    LazyOne lazy = LazyOne.getLazy();
                    System.out.println(lazy);
//                }
//            }).start();
//
//            latch.countDown();
        }

        long end = System.currentTimeMillis();
        System.out.println(" 线程结束,consumer： " + (end - start));
    }


}
