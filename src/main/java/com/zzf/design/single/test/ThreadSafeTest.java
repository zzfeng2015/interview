package com.zzf.design.single.test;

import com.zzf.design.single.hungry.Hungry;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

/**
 * 模拟并发
 * @author zzf
 * @date 2018/8/24 23:49.
 */
public class ThreadSafeTest {

    public static void main(String[] args) {
        int count = 100;
        CountDownLatch latch = new CountDownLatch(count);

        final Set<Hungry> synSet = Collections.synchronizedSet(new HashSet<>());

        for (int i = 0; i < count; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                }
            }).start();

            latch.countDown();
        }

        try {
            latch.await();//countdown = 0 ，释放
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(" 线程结束");
    }


}
