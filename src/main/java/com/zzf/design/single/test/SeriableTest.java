package com.zzf.design.single.test;

import com.zzf.design.single.seriali.Seriable;

import java.io.*;

/**
 * 模拟并发, 懒汉式不安全
 * @author zzf
 * @date 2018/8/24 23:49.
 */
public class SeriableTest {

    public static void main(String[] args) {

        Seriable s1 = null;
        Seriable s2 = Seriable.getInstance();

        FileOutputStream fos = null;

        try {
            fos = new FileOutputStream("seriable.obj");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(s2);

            oos.close();
            fos.close();

            ObjectInputStream ois = new ObjectInputStream(
                    new FileInputStream("seriable.obj")
            );
            s1 = (Seriable) ois.readObject();

            System.out.println(s1 == s2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

}
