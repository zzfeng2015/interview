package com.zzf.design.single.hungry;

/**
 * 类加载的时候，就立即初始化，并且创建单例对象
 *
 *  优点： 没有加任何锁，执行效率 比较高。
 *      线程安全
 *
 *  缺点：类加载的时候初始化，用不用，都占用空间内存
 *
 * @author zzf
 * @date 2018/8/24 23:45.
 */
public class Hungry {

    private Hungry() {
        //加载顺序
        //先静态 后动态
        //先属性 后方法
        //先上 后下
    }

    private static Hungry hungry = new Hungry();
    public static Hungry getHungry() {

        System.out.println(hungry);
        return hungry;
    }
}
