package com.zzf.design.single.register;

/**
 * @author zzf
 * @date 2018/8/25 00:29.
 */
public enum RegisterEnum {

    INSTANCE;

    public static RegisterEnum getInstance() {
        return INSTANCE;
    }
}
