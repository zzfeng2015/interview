package com.zzf.design.single.register;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 注册式
 * @author zzf
 * @date 2018/8/25 00:25.
 */
public class BeanFacotry {

    private static Map<String, Object> ioc = new ConcurrentHashMap<>();

    //TODO thread unsafe
    public static Object getBean(String className) {

        if (ioc.containsKey(className)) {
            return ioc.get(className);
        } else {
            try {
                Object instance = Class.forName(className).newInstance();
                ioc.put(className, instance);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ioc.get(className);

    }
}
