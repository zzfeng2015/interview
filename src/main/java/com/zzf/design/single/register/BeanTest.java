package com.zzf.design.single.register;

import java.util.concurrent.CountDownLatch;

/**
 * @author zzf
 * @date 2018/8/25 10:30.
 */
public class BeanTest {

    public static void main(String[] args) {

        int count = 100;

        CountDownLatch latch = new CountDownLatch(count);

        for (int i = 0; i < count; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        latch.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    Object bean = BeanFacotry.getBean("com.zzf.design.single.register.Pojo");
                    System.out.println(bean);
                }
            }).start();

            latch.countDown();
        }


    }
}
