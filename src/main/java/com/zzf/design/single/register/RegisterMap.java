package com.zzf.design.single.register;

import java.util.HashMap;
import java.util.Map;

/**
 * 注册式
 * @author zzf
 * @date 2018/8/25 00:25.
 */
public class RegisterMap {

    private static Map<String, Object> register = new HashMap();

    public static RegisterMap getInstance(String name) {

        if (name == null) {
            name = RegisterMap.class.getName();
        }

        if (register.get(name) == null) {
            try {
                register.put(name, RegisterMap.class.newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return (RegisterMap) register.get(name);
    }
}
