package com.zzf.design.single;

/**
 * @author zzf
 * @date 2018/8/24 22:44.
 */
public class Emperor {

    private static final Emperor emperor = new Emperor();
    private Emperor() {

    }

    public static Emperor getInstance() {
        return emperor;
    }

    public void say() {
        System.out.println("I am Empror..A. " + this);
    }
}
