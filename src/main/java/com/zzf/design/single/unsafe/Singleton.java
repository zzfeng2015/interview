package com.zzf.design.single.unsafe;

/**
 *  懒汉式
 * @author zzf
 * @date 2018/8/24 22:53.
 */
public class Singleton {

    private static Singleton singleton = null;

    private Singleton() {}

    //线程不安全
    public static Singleton getSingleton() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton ;
    }
}
