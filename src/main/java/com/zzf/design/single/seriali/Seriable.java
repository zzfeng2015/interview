package com.zzf.design.single.seriali;

import java.io.Serializable;

/**
 * 反序列化 导致 单例破坏
 * @author zzf
 * @date 2018/8/25 00:30.
 */
public class Seriable implements Serializable{

    /**
     *  序列化 把 内存中的状态 通过转换到字节码的形式。
     *  保存状态
     *
     *  反序列化
     *  将 已经持久化的字节码内容，转换成IO流，
     *  进而 将读取的内容转成 Java对象
     *  会重新 创建 对象
     *
     */
    public final static Seriable instance = new Seriable();

    private Seriable() {}

    public static Seriable getInstance() {
        return instance;
    }

    /**
     * jvm 实现，反序列化时使用
     * @return
     */
    private Object readResolve() {
        return instance;
    }



}
