package com.zzf.design.single.lazy;

/**
 * @author zzf
 * @date 2018/8/25 00:01.
 */
public class LazyTwo {

    private LazyTwo() {}

    private static LazyTwo lazy = null;

    /**
     * 加 Synchronized, 解决 thread safe
     *  性能 差！！！
     * @return
     */
    public static synchronized LazyTwo getLazy() {
        if (lazy == null) {
            lazy = new LazyTwo();
        }
        return lazy;
    }
}
