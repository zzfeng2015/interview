package com.zzf.design.single.lazy;

/**
 * 懒汉式 单例
 *      使用时才进行初始化
 * @author zzf
 * @date 2018/8/25 00:01.
 */
public class LazyOne {

    private LazyOne() {}

    //静态块，公共内存区域
    private static LazyOne lazy = null;

    public static LazyOne getLazy() {

        if (lazy == null) {
            lazy = new LazyOne();
        }
        return lazy;
    }
}
