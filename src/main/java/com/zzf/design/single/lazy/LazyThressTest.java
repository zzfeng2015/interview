package com.zzf.design.single.lazy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * 反射模拟调用两次 构造器
 * @author zzf
 * @date 2018/8/25 10:03.
 */
public class LazyThressTest {


    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {

        Class<LazyThree> clazz = LazyThree.class;

        Constructor<LazyThree> c = clazz.getDeclaredConstructor(null);

        c.setAccessible(true);

        LazyThree o1 = c.newInstance();

        LazyThree o2 = c.newInstance();



    }
}
