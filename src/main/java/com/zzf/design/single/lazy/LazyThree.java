package com.zzf.design.single.lazy;

/**
 * 特点： 在外部类被调用的时候内部类才会被加载
 *   内部类一定式要在方法调用前 初始化，
 *   巧妙解决线程安全
 *
 *
 *
 *   这种形式 兼顾 饿汉式的内存浪费，和synchronized 的性能问题
 *
 *   最牛b
 *
 *  内部类
 * @author zzf
 * @date 2018/8/25 00:14.
 */
public class LazyThree {

    //TODO 反射 改变值？？
    private static boolean initial = false;
    //默认使用LazyThree 的时候，会先初始化内部类
    //如果没有使用的化，内部类式不会加载的
    private LazyThree() {
        //防止反射入侵
        synchronized (LazyThree.class) {
            if (!initial) {
                initial = !initial;
            } else {
                throw new RuntimeException("单利侵入初始化");
            }
        }
    }

    //static: 共享
    //final： 保证该方法不会被重写，重载
    private static final LazyThree getInstance() {

        return LazyHolder.LAZY_THREE;
    }

    //默认不加载
    private static class LazyHolder {
        private static final LazyThree LAZY_THREE = new LazyThree();
    }
}
