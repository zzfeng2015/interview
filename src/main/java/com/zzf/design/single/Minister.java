package com.zzf.design.single;

/**
 * @author zzf
 * @date 2018/8/24 22:47.
 */
public class Minister {

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            Emperor emperor = Emperor.getInstance();
            emperor.say();
        }
    }
}
