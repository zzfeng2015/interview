package com.zzf.design.strategy;

/**
 * @author zzf
 * @date 2018/8/26 21:38.
 */
public class PayStrategyTest {

    public static void main(String[] args) {

        //订单
        Order order = new Order("1", "201808200000001", 100);

         //支付选择.微信，支付宝，京东白条
        //每个支付渠道有特定的算法。
        //基本算法固定

        //用户决定 支付渠道
//        PayState pay = order.pay(new AliPay());
        PayState pay = order.pay(PayType.JD_PAY);

        System.out.println(pay);
    }
}
