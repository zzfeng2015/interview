package com.zzf.design.strategy;

/**
 * @author zzf
 * @date 2018/8/26 21:34.
 */
public class Order {

    private String uid;
    private String orderId;
    private double amount;

    public Order(String uid, String orderId, double amount) {
        this.uid = uid;
        this.orderId = orderId;
        this.amount = amount;
    }

//    public PayState pay(Payment payment) {
//
//        return payment.pay(uid, amount);
//    }

    //完全可以用Payment接口代替， but why？
    //解决了 switch 的过程，和if else
    public PayState pay(PayType payType) {

        return payType.get().pay(uid, amount);
    }

}
