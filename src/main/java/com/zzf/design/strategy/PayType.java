package com.zzf.design.strategy;

/**
 * @author zzf
 * @date 2018/8/26 21:45.
 */
public enum PayType {

    ALI_PAY(new AliPay()),
    WECHAT_PAY(new WechatPay()),
    JD_PAY(new JDPay());

    private Payment payment;
    PayType(Payment payment) {
        this.payment = payment;
    }

    public Payment get() {
        return this.payment;
    }
}
