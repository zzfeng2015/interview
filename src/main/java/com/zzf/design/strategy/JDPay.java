package com.zzf.design.strategy;

/**
 * @author zzf
 * @date 2018/8/26 21:40.
 */
public class JDPay implements Payment {
    @Override
    public PayState pay(String uid, double amount) {
        System.out.println("AliPay 支付");
        System.out.println("开始扣款");
        return new PayState(200, "支付成功", null);
    }
}
