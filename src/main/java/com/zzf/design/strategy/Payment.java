package com.zzf.design.strategy;

/**
 * 支付渠道
 * @author zzf
 * @date 2018/8/26 21:34.
 */
public interface Payment {
    //TODO 每次新增支付渠道，就需要维护， enum
    public final static Payment ALI_PAY = new AliPay();
    public final static Payment WECHAT_PAY = new WechatPay();
    public final static Payment JD_PAY = new JDPay();



    PayState pay(String uid, double amount);

}
