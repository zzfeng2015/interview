package com.zzf.design.delegate;

import lombok.Data;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zzf
 * @date 2018/8/28 07:38.
 */
public class ServletDispatcher {

    private List<Handler> handlerList = new ArrayList<>();

    public void doService(HttpServletRequest request, HttpServletResponse reportse) {

    }



    @Data
    class Handler {
        private Object controller;
        private Method method;
        private String url;


    }
}
