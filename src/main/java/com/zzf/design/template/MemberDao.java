package com.zzf.design.template;

import java.sql.ResultSet;
import java.util.List;

/**
 * @author zzf
 * @date 2018/8/26 22:32.
 */
public class MemberDao  {

    //不继承，为了 解耦
    private JdbcTemplate jdbcTemplate = new JdbcTemplate(null);

    public List query() throws Exception {
        String sql = "select * from t_member";
        return jdbcTemplate.executeQuery(sql, new RowMapper<Member>() {

            //用户 写的地方
            @Override
            public Member mapRow(ResultSet rs, int rowNum) throws Exception {
                Member member = new Member();
                member.setUsername(rs.getString("username"));
                member.setPassword(rs.getString("password"));
                member.setAge(rs.getInt("age"));
                return member;
            }
        }, null);
    }

//    @Override
//    public Object processResult(ResultSet rs, int rowNum) throws SQLException {
//        Member member = new Member();
//        member.setUsername(rs.getString("username"));
//        member.setPassword(rs.getString("password"));
//        member.setAge(rs.getInt("age"));
//        return member;
//    }
}
