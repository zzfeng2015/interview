package com.zzf.design.template;

import java.sql.ResultSet;

/**
 * @author zzf
 * @date 2018/8/26 23:02.
 */
public interface RowMapper<T> {

    public T mapRow(ResultSet rs,int rowNum) throws Exception;
}
