package com.zzf.design.template;

import lombok.Data;

/**
 * @author zzf
 * @date 2018/8/26 22:31.
 */
@Data
public class Member {

    private String username;
    private String password;
    private String nickname;

    private int age;
}
