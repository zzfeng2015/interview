package com.zzf.design.factory.func;

import com.zzf.design.factory.Milk;
import com.zzf.design.factory.Yili;

/**
 * @author zzf
 * @date 2018/8/25 23:10.
 */
public class YiliFactotry implements Factory {
    @Override
    public Milk getMilk() {
        return new Yili();
    }
}
