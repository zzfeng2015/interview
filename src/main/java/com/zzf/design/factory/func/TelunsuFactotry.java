package com.zzf.design.factory.func;

import com.zzf.design.factory.Milk;
import com.zzf.design.factory.Telunsu;

/**
 * @author zzf
 * @date 2018/8/25 23:10.
 */
public class TelunsuFactotry implements Factory {
    @Override
    public Milk getMilk() {
        return new Telunsu();
    }
}
