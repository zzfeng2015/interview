package com.zzf.design.factory.func;

import com.zzf.design.factory.Mengniu;
import com.zzf.design.factory.Milk;

/**
 * @author zzf
 * @date 2018/8/25 23:10.
 */
public class MengniuFactotry implements Factory {
    @Override
    public Milk getMilk() {
        return new Mengniu();
    }
}
