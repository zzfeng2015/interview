package com.zzf.design.factory.func;

import com.zzf.design.factory.Milk;

/**
 * @author zzf
 * @date 2018/8/25 23:06.
 */
public interface Factory {

    //统一产品出口

    Milk getMilk();
}
