package com.zzf.design.factory.simple;

/**
 * @author zzf
 * @date 2018/8/25 22:56.
 */
public class Test {

    public static void main(String[] args) {


        SimpleFactory factory = new SimpleFactory();

        System.out.println(factory.getMilk("特仑苏"));
    }
}
