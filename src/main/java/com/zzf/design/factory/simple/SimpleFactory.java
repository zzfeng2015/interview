package com.zzf.design.factory.simple;

import com.zzf.design.factory.Mengniu;
import com.zzf.design.factory.Milk;
import com.zzf.design.factory.Telunsu;
import com.zzf.design.factory.Yili;

/**
 * 简单工厂模式
 *  小作坊
 * @author zzf
 * @date 2018/8/25 22:58.
 */
public class SimpleFactory {


    public Milk getMilk(String name) {

        if ("特仑苏".equals(name)) {
            return new Telunsu();
        } else if ("伊犁".equals(name)) {
            return new Yili();
        } else if ("蒙牛".equals(name)) {
            return new Mengniu();
        }
        return null;
    }
}
