package com.zzf.design.factory;

/**
 * @author zzf
 * @date 2018/8/25 22:54.
 */
public interface Milk {

    String getName();
}
