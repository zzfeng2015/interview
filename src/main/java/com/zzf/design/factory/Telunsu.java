package com.zzf.design.factory;

/**
 * @author zzf
 * @date 2018/8/25 22:56.
 */
public class Telunsu implements Milk {
    @Override
    public String getName() {
        return "特仑苏";
    }
}
