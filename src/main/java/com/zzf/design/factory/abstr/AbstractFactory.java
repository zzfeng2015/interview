package com.zzf.design.factory.abstr;

import com.zzf.design.factory.Milk;

/**
 * 抽象工厂， 用户的主入口
 * @author zzf
 * @date 2018/8/25 23:12.
 */
public abstract class AbstractFactory {

    public  abstract Milk getMengniu();
    public  abstract Milk getYili();
    public  abstract Milk getTelunsu();

}
