package com.zzf.design.factory.abstr;

import com.zzf.design.factory.Mengniu;
import com.zzf.design.factory.Milk;
import com.zzf.design.factory.Telunsu;
import com.zzf.design.factory.Yili;

/**
 * @author zzf
 * @date 2018/8/25 23:15.
 */
public class MilkFactory extends AbstractFactory {
    @Override
    public Milk getMengniu() {
        return new Mengniu();
    }

    @Override
    public Milk getYili() {
        return new Yili();
    }

    @Override
    public Milk getTelunsu() {
        return new Telunsu();
    }
}
