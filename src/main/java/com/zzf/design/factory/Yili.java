package com.zzf.design.factory;

/**
 * @author zzf
 * @date 2018/8/25 23:00.
 */
public class Yili implements Milk {
    @Override
    public String getName() {
        return "伊犁";
    }
}
