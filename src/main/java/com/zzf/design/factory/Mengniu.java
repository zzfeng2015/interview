package com.zzf.design.factory;

/**
 * @author zzf
 * @date 2018/8/25 23:01.
 */
public class Mengniu implements Milk {
    @Override
    public String getName() {
        return "蒙牛";
    }
}
