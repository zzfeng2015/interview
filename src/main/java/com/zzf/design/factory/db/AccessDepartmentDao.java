package com.zzf.design.factory.db;

/**
 * @author zzf
 * @date 2018/8/26 09:41.
 */
public class AccessDepartmentDao implements IDepartmentDao {
    @Override
    public void insert(DepartmentDo departmentDo) {
        System.out.println("SQL server 中 insert 一条 department 纪录");
    }

    @Override
    public DepartmentDo getDepartment(int id) {
        System.out.println("SQL server  中根据 id 得到一条 department 纪录");
        return null;
    }
}
