package com.zzf.design.factory.db;

/**
 * @author zzf
 * @date 2018/8/26 09:31.
 */
public class Main {

    public static void main(String[] args) {

        UserDO userDO = new UserDO();

        //变量1
        IFactory factory = new SqlServerFactory();
//        IFactory factory = new AccessFactory();

        IUserDao userDao = factory.createUser();

        userDao.insert(userDO);
        userDao.getUser(1);


        /**
         * 抽象工厂模式
         * 增加新的模块，需要添加
         * 1、IFacotry 新增 创建产品的不同种类接口， 修改 SqlServerFactory, AccessFactory
         * 2、新增 IDepartmentDao, SqlServerDepartment, AccessDepartment
         */
        IDepartmentDao departmentDao = factory.createDepartmentDao();

        departmentDao.insert(new DepartmentDo());
        departmentDao.getDepartment(1);

    }
}
