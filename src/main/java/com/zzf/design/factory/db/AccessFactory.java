package com.zzf.design.factory.db;

/**
 * @author zzf
 * @date 2018/8/26 09:30.
 */
public class AccessFactory implements IFactory {
    @Override
    public IUserDao createUser() {
        return new AccessUserDao();
    }

    @Override
    public IDepartmentDao createDepartmentDao() {
        return new AccessDepartmentDao();
    }
}
