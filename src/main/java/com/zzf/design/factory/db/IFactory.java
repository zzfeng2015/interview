package com.zzf.design.factory.db;

/**
 * @author zzf
 * @date 2018/8/26 09:26.
 */
public interface IFactory {

    IUserDao createUser();
    //新增department dao
    IDepartmentDao createDepartmentDao();
}
