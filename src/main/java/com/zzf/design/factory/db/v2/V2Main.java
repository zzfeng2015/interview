package com.zzf.design.factory.db.v2;

import com.zzf.design.factory.db.DepartmentDo;
import com.zzf.design.factory.db.IDepartmentDao;
import com.zzf.design.factory.db.IUserDao;
import com.zzf.design.factory.db.UserDO;

/**
 * @author zzf
 * @date 2018/8/26 10:02.
 */
public class V2Main {

    public static void main(String[] args) {
        UserDO user = new UserDO();
        DepartmentDo departmentDo = new DepartmentDo();

        /**
         *  缺点：
         *  1、db 硬编码
         *  2、添加 dao， 需要加case
         */
//        IUserDao userDao = DataAccess.creatUser();
        IUserDao userDao = DataAccess.createUserDaoV2();

        userDao.insert(user);
        userDao.getUser(1);

//        IDepartmentDao departmentDao = DataAccess.creatDepartment();
        IDepartmentDao departmentDao = DataAccess.createDepartmentDaoV2();

        departmentDao.insert(new DepartmentDo());
        departmentDao.getDepartment(1);
    }
}
