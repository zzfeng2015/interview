package com.zzf.design.factory.db;

/**
 * @author zzf
 * @date 2018/8/26 09:37.
 */
public interface IDepartmentDao {

    void insert(DepartmentDo departmentDo);

    DepartmentDo getDepartment(int id);
}
