package com.zzf.design.factory.db;

/**
 * 数据库 操作dao接口
 * @author zzf
 * @date 2018/8/26 09:26.
 */
public interface IUserDao {

    void insert(UserDO userDO);

    UserDO getUser(int id);

}
