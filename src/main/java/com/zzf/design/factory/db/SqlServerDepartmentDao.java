package com.zzf.design.factory.db;

/**
 * @author zzf
 * @date 2018/8/26 09:41.
 */
public class SqlServerDepartmentDao implements IDepartmentDao {
    @Override
    public void insert(DepartmentDo departmentDo) {
        System.out.println("access 中 insert 一条 department 纪录");
    }

    @Override
    public DepartmentDo getDepartment(int id) {
        System.out.println("access 中根据 id 得到一条 department 纪录");
        return null;
    }
}
