package com.zzf.design.factory.db;

/**
 * @author zzf
 * @date 2018/8/26 09:27.
 */
public class AccessUserDao implements IUserDao {
    @Override
    public void insert(UserDO userDO) {
        System.out.println(" Access 中  insert 一条 User 纪录");
    }

    @Override
    public UserDO getUser(int id) {
        System.out.println(" Access 中根据 id 得到一条 User 纪录");
        return null;
    }
}
