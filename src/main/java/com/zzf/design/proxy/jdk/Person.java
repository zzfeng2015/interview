package com.zzf.design.proxy.jdk;

/**
 * @author zzf
 * @date 2018/8/26 14:29.
 */
public interface Person {

    void findLove();
    void job();
}
