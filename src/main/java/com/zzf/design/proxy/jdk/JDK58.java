package com.zzf.design.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author zzf
 * @date 2018/8/26 14:40.
 */
public class JDK58 implements InvocationHandler{

    private Person target;

    public Object getInstance(Person target) {

        this.target = target;

        //生成新类
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                this);

    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println( "我是58，开始找工作 ");
        method.invoke(this.target,args);
        return null;
    }
}
