package com.zzf.design.proxy.jdk;

import sun.misc.ProxyGenerator;

import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author zzf
 * @date 2018/8/26 14:45.
 */
public class JDKProxyTest {


    public static void main(String[] args) throws IOException {

//        Person instance = (Person) new JDKMeipo().getInstance(new XieMu());
//        instance.findLove();

        Person proxy = (Person) new JDK58().getInstance(new XieMu());
        proxy.job();

        // 字节码重组原理
        //1、拿到被代理对象的引用，并获取它的所有接口，反射获取
        //2、jdk proxy 类重新生成一个新的类，同时新的类要实现被代理类所有实现方法
        //3、动态生成Java代码，把新加的业务逻辑方法由一定的逻辑代码去调用
        //4、编译新生成的java代码 .class
        //5、在重新加载到 jvm中运行

        byte[] bytes = ProxyGenerator.generateProxyClass("$Proxy0", new Class[]{Person.class});
        FileOutputStream fos = new FileOutputStream("$Proxy.class");

        fos.write(bytes);
        fos.close();

    }
}
