package com.zzf.design.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @author zzf
 * @date 2018/8/26 14:40.
 */
public class JDKMeipo implements InvocationHandler{

    private XieMu target;

    public Object getInstance(XieMu target) {

        this.target = target;

        //生成新类
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                this);

    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        System.out.println( "我是媒婆，已经拿到需求");
        System.out.println( "开始五色");
        method.invoke(this.target,args);
        return null;
    }
}
