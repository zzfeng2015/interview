package com.zzf.design.proxy.custom;

import java.lang.reflect.Method;

/**
 * @author zzf
 * @date 2018/8/26 15:13.
 */
public interface GPInvocationHandler {

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable;
}
