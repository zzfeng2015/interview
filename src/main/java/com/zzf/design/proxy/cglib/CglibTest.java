package com.zzf.design.proxy.cglib;

/**
 * @author zzf
 * @date 2018/8/26 14:57.
 */
public class CglibTest {

    public static void main(String[] args) {

        Zhangsan instance = (Zhangsan) new CglibMeipo().getInstance(new Zhangsan().getClass());

        instance.findLove();

        System.out.println(instance.getClass());
    }
}
