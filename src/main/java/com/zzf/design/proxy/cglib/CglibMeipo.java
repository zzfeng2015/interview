package com.zzf.design.proxy.cglib;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author zzf
 * @date 2018/8/26 14:53.
 */
public class CglibMeipo implements MethodInterceptor {

    public Object getInstance(Class clazz) {

        Enhancer enhancer = new Enhancer();
        //设置生成新类的父类
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(this);

        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
       //增强
        System.out.println( "我是媒婆，已经拿到需求");
        System.out.println( "开始五色");
        methodProxy.invokeSuper(o, objects);
        System.out.println("合适");

        return null;
    }
}
