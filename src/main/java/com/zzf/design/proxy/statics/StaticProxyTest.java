package com.zzf.design.proxy.statics;

/**
 * @author zzf
 * @date 2018/8/26 14:32.
 */
public class StaticProxyTest {

    public static void main(String[] args) {

        Son son = new Son();

        Father father = new Father(son);

        father.findLove();
    }
}
