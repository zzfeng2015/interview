package com.zzf.design.proxy.statics;

/**
 * @author zzf
 * @date 2018/8/26 14:30.
 */
public class Father {

    private Son person;

    //强依赖，不能扩展
    public Father(Son person) {
        this.person = person;
    }

    public void findLove() {

        System.out.println("开始五色");
        this.person.findLove();
        System.out.println("结束");
    }
}
