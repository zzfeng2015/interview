package com.zzf.design.prototype;


import java.io.*;

/**
 * @author zzf
 * @date 2018/8/25 09:16.
 */
public class Resume implements Cloneable, Serializable{

    private String name;
    private String sex;
    private String age;
    private String timeArea;
    private String company;

    private WorkExperience work;

    public Resume(String name) {
        this.name = name;
        work = new WorkExperience();
    }

    public void setPersonalInfo(String sex, String age) {
        this.sex = sex;
        this.age = age;
    }

    public void setWorkExperience(String timeArea, String company) {
        this.timeArea = timeArea;
        this.company = company;

        //深 拷贝
        work.setCompany(company);
        work.setWorkDate(timeArea);
    }

    public void display() {
        System.out.println(String.join(" ", name, sex, age));
        System.out.println("工作经历：" + String.join(" ", timeArea, company                                                                                                 ));
        System.out.println("workExperience：" + String.join(" ", work.getCompany(), work.getWorkDate()                                                                                                 ));
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Object deepClone() throws IOException, ClassNotFoundException {
        // 序列化

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(this);

        ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bis);
        Resume o = (Resume) ois.readObject();
        return o;
    }

    public Object deepClone2() throws Exception {

        Resume clone = (Resume) super.clone();
        clone.work = (WorkExperience) this.work.clone();
        return clone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getTimeArea() {
        return timeArea;
    }

    public void setTimeArea(String timeArea) {
        this.timeArea = timeArea;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
