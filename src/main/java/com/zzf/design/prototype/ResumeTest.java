package com.zzf.design.prototype;

/**
 * @author zzf
 * @date 2018/8/25 09:30.
 */
public class ResumeTest {

    public static void main(String[] args) throws Exception {

        //浅复制
        Resume resume = new Resume("fred");
        resume.setPersonalInfo("male", "29");
        resume.setWorkExperience("1990-2008", "company A");

//        Resume b = (Resume) resume.deepClone();
        Resume b = (Resume) resume.deepClone2();
//        Resume b = (Resume) resume.clone();

        System.out.println("resume == b？" + (resume == b));
        System.out.println("resume.getClass() == b.getClass()？" + (resume.getClass() == b.getClass()));
        b.setWorkExperience("2000-2015"," tongdun");

        Resume c = (Resume) resume.clone();
        c.setPersonalInfo("male", "30");

        System.out.println("===============");
        resume.display();
        System.out.println("===============");
        b.display();
        System.out.println("===============");
        c.display();

        System.out.println("=====deep clone ====");

    }
}
