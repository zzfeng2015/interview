package com.zzf.design.prototype;

/**
 * @author zzf
 * @date 2018/8/25 09:13.
 */
public class CloneTest {

    public static void main(String[] args) throws CloneNotSupportedException {
        Email email = new Email();
        email.setName("fred");
        email.setContent("8:00 开会");

        Email clone = (Email) email.clone();
        System.out.println(clone);
        System.out.println(email);

        clone.setContent("8:30 开会");
        System.out.println(clone);
        System.out.println(email);
    }
}
