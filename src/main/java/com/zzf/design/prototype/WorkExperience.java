package com.zzf.design.prototype;

import java.io.Serializable;

/**
 * @author zzf
 * @date 2018/8/25 10:44.
 */
public class WorkExperience implements Serializable,Cloneable{

    private String workDate;
    private String company;


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getWorkDate() {
        return workDate;
    }

    public void setWorkDate(String workDate) {
        this.workDate = workDate;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
