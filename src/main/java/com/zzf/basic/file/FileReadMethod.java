package com.zzf.basic.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * @author zzf
 * @date 2018/8/27 14:12.
 */
public class FileReadMethod {

    public static void main(String[] args) throws IOException {

//        loadFromClassLoad();

//        loadFile();

        loadUrl();
//        loadResourceBund();

    }

    public static void loadUrl() throws IOException {
        String path = FileReadMethod.class.getResource("").getPath();
        System.out.println(path);
//        URL url = FileReadMethod.class.getResource("/Users/zzf/all-projects/interview/src/main/resources/db.properties");
//        InputStream inStream = url.openStream();
//
//        Properties properties = new Properties();
//        properties.load(inStream);
//
//        System.out.println(properties.getProperty("db"));
    }


    public static void loadResourceBund() {
        //TODO
        //src 下 直接 db
        ResourceBundle resource = ResourceBundle.getBundle("com/zzf/db");//test为属性文件名，放在包com.mmq下，如果是放在src下，直接用test即可
        String key = resource.getString("db");
        System.out.println(key);
    }

    public static void loadFile() throws IOException {

        InputStream inputStream = new FileInputStream(new File("src/main/resources/db.properties"));
        Properties properties = new Properties();
        properties.load(inputStream);

        System.out.println(properties.getProperty("db"));
    }

    /**
     * classLoader 加载
     * @throws IOException
     */
    public static void loadFromClassLoad() throws IOException {

//        InputStream inputStream = FileReadMethod.class.getClassLoader().getResourceAsStream("db.properties");
        InputStream inputStream = ClassLoader.getSystemResourceAsStream("db.properties");

        Properties properties = new Properties();
        properties.load(inputStream);

        System.out.println(properties.getProperty("db"));

    }
}
